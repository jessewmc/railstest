require 'sinatra'

class Babyapp < Sinatra::Base
	get '/test/*' do
		params[:splat]
	end
	get '/form' do
		erb :form
	end
	post '/form' do
		params[:message]
	end
	not_found do
		status 404
		'not here sorry'
	end
end