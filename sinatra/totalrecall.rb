require 'rubygems'
require 'bundler/setup'

Bundler.require

#require 'sinatra'
#require 'data_mapper'
#require 'sinatra/flash'

enable :sessions

DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/eia.db")

class Note
	include DataMapper::Resource #this is a mixin
	property :id, Serial #this is defrault primary key, :key => true
	property :program_number, Integer, :required => true
	property :sub_programs, Text
	property :content, Text, :required => true
	property :complete, Boolean, :required => true, :default => false
	property :created_at, DateTime
	property :updated_at, DateTime
end

DataMapper.finalize.auto_upgrade!

helpers do
	include Rack::Utils #mixin :escape_html
end

get '/' do
	@notes = Note.all :order => :program_number.desc
	@title  = 'All  Notes'
	if @notes.empty?
		flash.now[:error] = 'No notes found. Add your first below.'
	end
	erb :home
end

#todo: handle missing required entry
post '/' do
	n = Note.new
	n.content  = params[:content]
	n.program_number = params[:program_number]
	n.sub_programs = params[:sub_programs]
	n.created_at = Time.now
	n.updated_at = Time.now
	if n.save
		redirect '/', flash[:notice] = 'Entry created successfully'
	else
		redirect '/', flash[:error] = 'Entry error'
	end
	redirect '/'
end

get '/:id' do
	@note = Note.get params[:id]
	@title = "Edit note ##{params[:id]}"
	erb :edit
end

put '/:id' do
	n = Note.get params[:id]
	n.content = params[:content]
	n.complete = params[:complete] ? 1 : 0
	n.updated_at = Time.now
	n.save
	redirect '/'
end

get '/:id/delete' do
	@note = Note.get params[:id]
	@title = "Confirm deletion of note ##params[:id]}"
	erb :delete
end

delete '/:id' do
	n = Note.get params[:id]
	n.destroy
	redirect '/'
end

get '/:id/complete' do
	n = Note.get params[:id]
	n.complete = n.complete ? 0 : 1
	n.updated_at = Time.now
	n.save
	redirect '/'
end