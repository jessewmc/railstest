=begin
ugly multiline comment
TODO:
pagination
tags
sub programs
search
=end

require 'rubygems'
require 'bundler/setup'

require 'sinatra'
require 'data_mapper'
require 'sinatra/flash'

enable :sessions

#DataMapper::setup(:default, "sqlite3://#Dir.pwd}/eia.db")
DataMapper::setup(:default, "sqlite3:eia.db")

class Program

	class Link
		include DataMapper::Resource
		
		storage_names[:default] = 'program_links' #why is this here?
		
		belongs_to :sub, 'Program', :key => true
		belongs_to :sup, 'Program', :key => true
	end
	
	include DataMapper::Resource
	
	property :program_number, Integer, :key => true
	property :name, String
	property :comment, Text
	property :eia_code, Text
	property :created_at, DateTime
	property :updated_at, DateTime
	
	has n, :links_to_sub_programs, 'Program::Link', :child_key => [:sup_program_number]
	has n, :links_to_sup_programs, 'Program::Link', :child_key => [:sub_program_number]
	
	has n, :sub_programs, self, :through => :links_to_sub_programs, :via => :sub
	
	has n, :sup_programs, self, :through => :links_to_sup_programs, :via => :sup
	
	has n, :tags, :through => Resource
	
	def add_sub(subs)
		sub_programs.concat(Array(subs))
		save
		self
	end
	
	def remove_sub(subs)
		links_to_sub_programs.all(:sub => Array(subs)).destroy!
		reload
		self
	end
end

class Tag
	include DataMapper::Resource
	
	property :ctag, String, :key => true
	has n, :programs, :through => Resource
end

DataMapper.finalize.auto_upgrade!

get '/' do
	@programs = Program.all :order => :program_number.desc
	@tags = Tag.all :order => :ctag.desc
	@title = 'All Programs'
	if @programs.empty?
		flash.now[:error] = 'no programs in database'
	end
	erb :home
end

post '/new' do
	p = Program.new
	p.program_number = params[:program_number]
	p.name = params[:name]
	p.comment = params[:comment]
	p.eia_code = params[:eia_code]
	p.created_at = Time.now
	p.updated_at = Time.now
	
	if p.save
		redirect '/', flash[:notice] = "program #{params[:program_number]} added to database"
	else
		redirect '/', flash[:error] = 'database entry error'
	end
end

get '/new' do
	@title = 'new program'
	erb :new
end
	

get '/:program_number' do
	@program = Program.get params[:program_number]
	@title = "program #{params[:program_number]}"
	erb :edit
end

get '/:program_number/delete' do
	@program = Program.get params[:program_number]
	@title = "Delete program #{params[:program_number]}"
	erb :delete
end

get '/:program_number/sub' do
	@program = Program.get params[:program_number]
	@title = "Add sub programs to program #{params[:program_number]}"
	erb :sub
end

put '/:program_number/sub' do
	p = Program.get params[:program_number]
	sub = Program.get params[:sub]
	p.add_sub(sub)
	redirect "/#{params[:program_number]}/sub", flash[:notice] = "sub program #{params[:sub]} added to program #{params[:program_number]}"
end

get '/tag/:tag' do
	@tag = Tag.get params[:tag]
	@title = "Programs tagged #{params[:tag]}"
	erb :tagged
end

get '/:program_number/tag' do
	@program = Program.get params[:program_number]
	@title = "Add tags to program #{params[:program_number]}"
	erb :tag
end

put '/:program_number/tag' do
	p = Program.get params[:program_number]
	tag = Tag.get(params[:tag]) || Tag.new(:ctag => params[:tag])
	tag.programs << p
	p.tags << tag
	p.save
	if tag.save
		redirect "/#{params[:program_number]}/tag", flash[:notice] = "tag \'#{params[:tag]}' saved to program #{params[:program_number]}"
	else
		redirect "/#{params[:program_number]}/tag", flash[:error] = "error saving tag '#{params[:tag]}'"
	end
end

delete '/:program_number/tag/:tag' do
	p = Program.get params[:program_number]
	tag = Tag.get params[:tag]
	p.tags.delete(tag)
	p.tags.save
	if tag.programs.empty?
		tag.destroy
	end
	redirect "/#{params[:program_number]}/tag", flash[:notice] = "tag #{params[:tag]} removed"
end

delete '/:program_number/sub/:sub' do
	p = Program.get params[:program_number]
	sub = Program.get params[:sub]
	p.remove_sub(sub)
	redirect "/#{params[:program_number]}/sub", flash[:notice] = "subprogram #{params[:sub]} removed from program #{params[:program_number]}"
end

delete '/:program_number' do
	p = Program.get params[:program_number]
	p.destroy
	redirect '/'
end

put '/:program_number' do
	p = Program.get params[:program_number]
	p.comment = params[:comment]
	p.name = params[:name]
	p.eia_code = params[:eia_code]
	p.updated_at = Time.now
	p.save
	redirect '/'
end